#ifndef _GISTRE_UTILS_H
#define _GISTRE_UTILS_H


/*
 * Code Recupere de ma piscine C, pour le moment il est sale, mais je vais le rendre propre.
 */

int my_natoi(const char *str, int n);


/*
 * Char buffer for pretty print in kernel log.
 */

struct str_buffer {
    size_t size;
    size_t max_size;
    char *str;
};

struct str_buffer *str_buffer_init(size_t max_size);
void str_buffer_del(struct str_buffer *buf);
void str_buffer_flush(struct str_buffer *buf);
void str_buffer_clear(struct str_buffer *buf);
size_t str_buffer_push(struct str_buffer *buf, size_t len_fmt, char *fmt, unsigned int value);

#endif /* _GISTRE_UTILS_H */
