#include <linux/cdev.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/regmap.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/mod_devicetable.h>

#include "mfrc522_emu.h"
#include "mfrc522.h"
#include "gistre_utils.h"


MODULE_AUTHOR("Alexis HUARD - Leo CARDAO");
MODULE_LICENSE("GPL v2");
MODULE_SOFTDEP("pre: mfrc522_emu");

#define BUFFER_SIZE 25 + 1
#define DEBUG_BUFFER_SIZE 15
#define NODE_NAME "mfrc522_emu"
#define DRIVER_NAME "gistre_card"

/**
  *
  * Forward declarations
  *
**/

ssize_t gistre_read(struct file*, char __user*, size_t, loff_t*);
ssize_t gistre_write(struct file*, const char __user*, size_t, loff_t*);
int gistre_open(struct inode*, struct file*);



struct gistre_drv {
    struct cdev cdev;
    struct mfrc522_dev *mfrc522;
    char internal_buffer[BUFFER_SIZE];
    bool debug_mode;
    struct str_buffer *debug_buffer;
};


//static struct gistre_drv *gistre_drv;
static int major;

static struct gistre_drv *drv;

static const struct file_operations fops = {
    .write = gistre_write,
    .read = gistre_read,
    .open = gistre_open,
    .owner = THIS_MODULE
};


static inline unsigned long alignup(unsigned long x, unsigned long y)
{
    return x + (y - x) % y;
}


static inline void print_size_fifo(struct regmap *reg) {
    int tmp;
    regmap_read(reg, MFRC522_FIFOLEVELREG, &tmp);
    pr_info("tmp:%d\n", tmp);
}


static void mem_write(uint8_t *str, size_t size, struct gistre_drv *drv) {
    struct regmap *reg;
    size_t trailing;
    size_t i;

    reg = mfrc522_get_regmap(drv->mfrc522);
    trailing = alignup(size, 25) - size;

    if (drv->debug_mode == true)
        pr_info("<WR>\n");
    for (i = 0; i < size; ++i) {
        regmap_write(reg, MFRC522_FIFODATAREG, str[i]);
        if (drv->debug_mode == true)
            str_buffer_push(drv->debug_buffer, 3, "%02x ", str[i]);
    }
    for (i = 0; i < trailing; ++i) {
        regmap_write(reg, MFRC522_FIFODATAREG, '\0');
        if (drv->debug_mode == true)
            str_buffer_push(drv->debug_buffer, 3, "%02x ", '\0');
    }
    regmap_write(reg, MFRC522_CMDREG, MFRC522_MEM);
    if (drv->debug_mode == true)
        str_buffer_flush(drv->debug_buffer);
}


static void mem_read(struct gistre_drv *drv) {
    struct regmap *reg;
    size_t i;
    int tmp;

    reg = mfrc522_get_regmap(drv->mfrc522);
    regmap_write(reg, MFRC522_FIFOLEVELREG, MFRC522_FIFOLEVELREG_FLUSH);
    regmap_write(reg, MFRC522_CMDREG, MFRC522_MEM);

    if (drv->debug_mode == true)
        pr_info("<RD>\n");
    for(i = 0; i < 25; ++i) {
        regmap_read(reg, MFRC522_FIFODATAREG, &tmp);
        drv->internal_buffer[i] = (char)tmp;
        if (drv->debug_mode == true) {
            str_buffer_push(drv->debug_buffer, 3, "%02x ", tmp);
        }
    }
    if (drv->debug_mode == true)
        str_buffer_flush(drv->debug_buffer);
}


static void gen_rand_id(void) {
    struct regmap *reg;

    reg = mfrc522_get_regmap(drv->mfrc522);
    regmap_write(reg, MFRC522_CMDREG, MFRC522_GENERATERANDOMID);
}


int gistre_open(struct inode *inode, struct file *file) {
    file->private_data = (void*)drv;
    return 0;
}


ssize_t gistre_read(struct file *file, char __user *buf,
                    size_t len, loff_t *off) {
    struct gistre_drv *gistre_drv;
    ssize_t size;

    gistre_drv = (struct gistre_drv*)file->private_data;

    if (gistre_drv->internal_buffer[0] != '\0') {
        if (copy_to_user(buf, gistre_drv->internal_buffer, BUFFER_SIZE - 1) != 0) {
            return -1;
        }
        else {
            size = BUFFER_SIZE;
            memset(gistre_drv->internal_buffer, '\0', BUFFER_SIZE);
        }
    }
    else {
        size = 0;
    }
    return size;
}


ssize_t gistre_write(struct file *file, const char __user *buf,
                    size_t len, loff_t *off) {
    struct gistre_drv *drv;
    char *msg_buffer;
    int str_len;
    size_t old_size, size;

    drv = (struct gistre_drv*)file->private_data;
    msg_buffer = kmalloc(sizeof(char) * len, GFP_KERNEL);
    memset(msg_buffer, '\0', len);

    if (copy_from_user(msg_buffer, buf, len) != 0) {
        return -1;
    }

    size = strcspn(msg_buffer, ":");

    if (strncmp(msg_buffer, "mem_write", size) == 0){
        old_size = size;
        size = strcspn(msg_buffer + size + 1, ":");
        str_len = my_natoi(msg_buffer+old_size+1, size);
        size = old_size + size + 1 + 1;
        mem_write(msg_buffer + size, str_len, drv);
    }
    else if (strncmp(msg_buffer, "debug", size) == 0) {
        if (strncmp(msg_buffer + size + 1, "on", 2) == 0) {
            drv->debug_mode = true;
            if (!drv->debug_buffer)
                drv->debug_buffer = str_buffer_init(DEBUG_BUFFER_SIZE);
        }
        else if (strncmp(msg_buffer + size + 1, "off", 3) == 0) {
            drv->debug_mode = false;
            if (drv->debug_buffer)
                str_buffer_del(drv->debug_buffer);
        }
    }
    else if (strncmp(msg_buffer, "mem_read", size) == 0) {
        mem_read(drv);
    }
    else if (strncmp(msg_buffer, "gen_rand_id", size) == 0){
        gen_rand_id();
    }
    else {
        return -1;
    }
    return len;
}


static const struct of_device_id gistre_ids[] = {
    { .compatible = DRIVER_NAME, },
    { },
};
MODULE_DEVICE_TABLE(of, gistre_ids);


static struct platform_driver gistre_pdrv = {
    .driver = {
        .name = DRIVER_NAME,
        .owner = THIS_MODULE,
        .of_match_table = gistre_ids,
    },
};


__init
static int init_hello(void)
{
    dev_t dev;
    int ret;
    void *tmp;
    struct device_node *node;
    u32 version;
    const char *name = DRIVER_NAME;

    if (alloc_chrdev_region(&dev, 0, 1, name)) {
        return -1;
    }
    else {
        major = MAJOR(dev);
        pr_info("Major is %d\n", major);
    }

    drv = kmalloc(sizeof(struct gistre_drv), GFP_KERNEL);
    if (!drv) {
        return -1;
    }
    memset(drv->internal_buffer, '\0', BUFFER_SIZE);
    drv->debug_mode = false;
    cdev_init(&drv->cdev, &fops);
    tmp = mfrc522_find_dev();
    if (!tmp)
        return -ENODEV;
    drv->mfrc522 = dev_to_mfrc522(tmp);
    if (!drv->mfrc522)
        return -ENODEV;
    ret = cdev_add(&drv->cdev, dev, 1);
    if (ret < 0)
        return ret;

    platform_driver_register(&gistre_pdrv);
    node = of_find_node_by_name(of_root, NODE_NAME);
    if (!node)
        pr_err("Node not found\n");
    
    if (of_property_read_u32(node, MFRC522_PROP_VERSION, &version))
        pr_err("Version not found\n");
    else
        pr_info("DEVICE_TREE - %s : %d\n", MFRC522_PROP_VERSION, version);

    pr_info("Hello GISTRE card!\n");
    return 0;
}


__exit
static void exit_hello(void)
{
    dev_t dev;

    cdev_del(&drv->cdev);

    kfree(drv);
    dev = MKDEV(major, 0);
    unregister_chrdev_region(dev, 1);
    pr_info("By bye GISTRE card!\n");
}


module_init(init_hello);
module_exit(exit_hello);

