#include <linux/slab.h>

#include "gistre_utils.h"

int my_natoi(const char *str, int n) {
    int result = 0;
    int k = 1;
    int i = 0;
    for (; str[i] && str[i] == ' '; i++)
        continue;
    if (!str)
        return 0;
    if (str[i] == '+')
        i++;
    else if (str[i] == '-') {
        i++;
        k = -1;
    }
    for (; str[i + 1] && i + 1 < n; i++) {
        if (str[i] >= '0' && str[i] <= '9')
            result = (result + str[i] - '0') * 10;
        else
            return 0;
    }
    return (result + (str[i] - '0')) * k;
}


struct str_buffer *str_buffer_init(size_t max_size) {
    struct str_buffer *ret;

    ret = kcalloc(1, sizeof(struct str_buffer), GFP_KERNEL);
    if (!ret)
        return NULL;
    ret->max_size = max_size;
    ret->str = kcalloc(max_size + 1, sizeof(char), GFP_KERNEL);
    return ret;
}


void str_buffer_del(struct str_buffer *buf) {
    if (buf->str)
        kfree(buf->str);
    kfree(buf);
}


void str_buffer_flush(struct str_buffer *buf) {
    pr_info("%s\n", buf->str);
    buf->size = 0;
}


void str_buffer_clear(struct str_buffer *buf) {
    buf->size = 0;
}


size_t str_buffer_push(struct str_buffer *buf, size_t len_fmt, char *fmt, unsigned int value) {
    int ret;

    if (buf->size + len_fmt > buf->max_size)
        str_buffer_flush(buf);
    ret = snprintf(buf->str + buf->size, buf->max_size - buf->size, fmt, value);
    buf->size = buf->size + ret;
    return ret;
}
